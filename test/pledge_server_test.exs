defmodule PledgerTest do
  use ExUnit.Case

  alias CutieServer.Servers.Pledger

  test "caches the 3 most recent pledges and totals their amounts" do
    Pledger.start()

    Pledger.create_pledge("larry", 10)
    Pledger.create_pledge("moe", 20)
    Pledger.create_pledge("curly", 30)
    Pledger.create_pledge("daisy", 40)
    Pledger.create_pledge("grace", 50)

    most_recent_pledges = [{"grace", 50}, {"daisy", 40}, {"curly", 30}]

    assert Pledger.recent_pledges() == most_recent_pledges

    assert Pledger.total_pledged() == 120
  end
end
