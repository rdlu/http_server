defmodule HandlerTest do
  use ExUnit.Case

  import CutieServer.Handler, only: [handle: 1]

  test "GET /pets" do
    request = """
    GET /pets HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = handle(request)

    assert response == """
           HTTP/1.1 200 OK\r
           Content-Type: text/html\r
           Content-Length: 13\r
           \r
           Cats and Dogs
           """
  end

  test "GET /cats" do
    request = """
    GET /cats HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = handle(request)

    expected_response = """
    HTTP/1.1 200 OK\r
    Content-Type: text/html\r
    Content-Length: 204\r
    \r
    <h1>Listing all cats from CatCafe</h1>
    <ul>

      <li id="cat-1">Margot - Mixed-White</li>

      <li id="cat-2">Liz - Black</li>

      <li id="cat-3">Eric - Yellow</li>

      <li id="cat-4">Marie - White</li>

    </ul>
    """

    assert remove_whitespace(response) == remove_whitespace(expected_response)
  end

  test "GET /koalas" do
    request = """
    GET /koalas HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = handle(request)

    assert response == """
           HTTP/1.1 404 Not Found\r
           Content-Type: text/html\r
           Content-Length: 19\r
           \r
           No path for /koalas
           """
  end

  test "GET /cats/1" do
    request = """
    GET /cats/1 HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = handle(request)

    expected_response = """
    HTTP/1.1 200 OK\r
    Content-Type: text/html\r
    Content-Length: 82\r
    \r
    <h1 id=\"cat-1\">Cat Margot!</h1>
    <p>
    Is Margot sleeping?<strong>true</strong>
    </p>
    """

    assert remove_whitespace(response) == remove_whitespace(expected_response)
  end

  test "GET /about" do
    request = """
    GET /about HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = handle(request)

    expected_response = """
    HTTP/1.1 200 OK\r
    Content-Type: text/html\r
    Content-Length: 340\r
    \r
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>About Cutie Http Server</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </head>
      <body>
        <h1>It works!</h1>
        <p>Loading about.html from pages</p>
      </body>
    </html>
    """

    assert remove_whitespace(response) == remove_whitespace(expected_response)
  end

  test "POST /cats" do
    request = """
    POST /cats HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/x-www-form-urlencoded\r
    Content-Length: 33\r
    \r
    name=Yuna&type=Brown
    """

    response = handle(request)

    assert response == """
           HTTP/1.1 201 Created\r
           Content-Type: text/html\r
           Content-Length: 33\r
           \r
           Adopted a new cat! Welcome, Yuna!
           """
  end

  test "GET /api/cats" do
    request = """
    GET /api/cats HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    \r
    """

    response = handle(request)

    expected_response = """
    HTTP/1.1 200 OK\r
    Content-Type: application/json\r
    Content-Length: 228\r
    \r
    [{"type":"Mixed-White","sleeping":true,"name":"Margot","id":1},
    {"type":"Black","sleeping":false,"name":"Liz","id":2},
    {"type":"Yellow","sleeping":true,"name":"Eric","id":3},
    {"type":"White","sleeping":false,"name":"Marie","id":4}]
    """

    assert remove_whitespace(response) == remove_whitespace(expected_response)
  end

  test "POST /api/cats" do
    request = """
    POST /api/cats HTTP/1.1\r
    Host: example.com\r
    User-Agent: ExampleBrowser/1.0\r
    Accept: */*\r
    Content-Type: application/json\r
    Content-Length: 21\r
    \r
    {"name": "Niece", "type": "Black"}
    """

    response = handle(request)

    assert response == """
           HTTP/1.1 201 Created\r
           Content-Type: application/json\r
           Content-Length: 40\r
           \r
           {"type":"Black","name":"Niece","id":777}
           """
  end

  defp remove_whitespace(text) do
    String.replace(text, ~r{\s}, "")
  end
end
