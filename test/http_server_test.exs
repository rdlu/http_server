defmodule HttpServerTest do
  use ExUnit.Case

  alias HttpServer
  spawn(HttpServer, :start, [4000])

  test "accepts a request on a socket and sends back a response" do
    {:ok, response} = HTTPoison.get("http://localhost:4000/pets")

    assert response.status_code == 200
    assert response.body == "Cats and Dogs"
  end

  test "accepts multiple requests on a socket and sends back a response" do
    max_concurrent_requests = 5
    url = "http://localhost:4000/pets"

    1..max_concurrent_requests
    |> Enum.map(fn _ -> Task.async(fn -> HTTPoison.get(url) end) end)
    |> Enum.map(&Task.await/1)
    |> Enum.map(&assert_successful_response/1)
  end

  defp assert_successful_response({:ok, response}) do
    assert response.status_code == 200
    assert response.body == "Cats and Dogs"
  end
end
