defmodule ParserTest do
  use ExUnit.Case
  doctest CutieServer.Parser

  alias CutieServer.Parser

  test "parses a list of header fields into a map" do
    header_lines = ["First: 1", "Second: application-test"]
    headers = Parser.parse_headers(header_lines)

    assert headers == %{"First" => "1", "Second" => "application-test"}
  end
end
