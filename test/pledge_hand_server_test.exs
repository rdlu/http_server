defmodule PledgeHandTest do
  use ExUnit.Case

  alias CutieServer.Servers.PledgeHand

  test "caches the 3 most recent pledges and totals their amounts" do
    PledgeHand.start()

    PledgeHand.create_pledge("larry", 10)
    PledgeHand.create_pledge("moe", 20)
    PledgeHand.create_pledge("curly", 30)
    PledgeHand.create_pledge("daisy", 40)
    PledgeHand.create_pledge("grace", 50)

    most_recent_pledges = [{"grace", 50}, {"daisy", 40}, {"curly", 30}]

    assert PledgeHand.recent_pledges() == most_recent_pledges

    assert PledgeHand.total_pledged() == 120
  end
end
