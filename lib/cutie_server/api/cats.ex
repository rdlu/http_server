defmodule CutieServer.Api.Cats do
  alias CutieServer.Repositories.CatCafe
  alias CutieServer.RequestMap

  def index(%RequestMap{} = map) do
    map |> render(CatCafe.list_cats())
  end

  def create(%RequestMap{} = map, %{"name" => name, "type" => type}) do
    map |> render(%{"name" => name, "type" => type, "id" => 777}, 201)
  end

  defp render(map, data, status \\ 200) do
    json = data |> Poison.encode!()
    map = content_type(map, "application/json")

    %RequestMap{map | status: status, resp_body: json}
  end

  defp content_type(map, type) do
    headers = Map.put(map.resp_headers, "Content-Type", type)
    %{map | resp_headers: headers}
  end
end
