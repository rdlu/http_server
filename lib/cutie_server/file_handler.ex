defmodule CutieServer.FileHandler do
  def handle_file({:ok, content}, map) do
    %{map | status: 200, resp_body: content}
  end

  def handle_file({:error, :enoent}, map) do
    %{map | status: 404, resp_body: "File Not Found"}
  end

  def handle_file({:error, reason}, map) do
    %{map | status: 500, resp_body: "File error: #{reason}"}
  end
end
