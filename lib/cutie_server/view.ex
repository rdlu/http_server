defmodule CutieServer.View do
  @templates_path Path.expand("templates", File.cwd!())

  def render(map, template, bindings \\ []) do
    content =
      @templates_path
      |> Path.join(template <> ".html.eex")
      |> EEx.eval_file(bindings)

    %{map | status: 200, resp_body: content}
  end
end
