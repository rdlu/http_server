defmodule CutieServer.Repositories.CatCafe do
  alias CutieServer.Schemas.Cat

  @db_path Path.expand("db", File.cwd!())

  def list_cats do
    @db_path
    |> Path.join("cats.json")
    |> read_json
    |> Map.get("cats")
  end

  def get_cat(id) when is_integer(id) do
    Enum.find(list_cats(), &Cat.by_id(&1, id))
  end

  def get_cat(id) when is_binary(id) do
    id |> String.to_integer() |> get_cat
  end

  def mixed_cats do
    list_cats()
    |> Enum.filter(&Cat.is_mixed/1)
    |> Enum.sort_by(& &1.name)
  end

  defp read_json(source) do
    with {:ok, content} <- File.read(source) do
      Poison.decode!(content, as: %{"cats" => [%Cat{}]})
    else
      {:error, reason} ->
        IO.inspect("Error reading #{source}: #{reason}")
    end
  end
end
