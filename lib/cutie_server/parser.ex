defmodule CutieServer.Parser do
  alias CutieServer.RequestMap

  def parse(request) do
    [top, params_string] = request |> String.split("\r\n\r\n")

    [route | header_string] = top |> String.split("\r\n")

    [method, path, _version] = route |> String.split()

    headers = parse_headers(header_string)

    %RequestMap{
      method: method,
      path: path,
      resp_body: "",
      status: nil,
      params: parse_params(headers["Content-Type"], params_string),
      headers: headers
    }
  end

  @doc """
  Parses a given param string in URI form `key1=value1&key2=value2`
  into a RequestMap with corresponding keys and values

  ## Examples
    iex> params_string = "name=Test&type=Typed"
    iex> CutieServer.Parser.parse_params("application/x-www-form-urlencoded", params_string)
    %{"name" => "Test", "type" => "Typed"}
    iex> CutieServer.Parser.parse_params("multipart/form_data", params_string)
    %{}
  """
  def parse_params("application/x-www-form-urlencoded", params) do
    params |> String.trim() |> URI.decode_query()
  end

  def parse_params("application/json", params_string) do
    Poison.Parser.parse!(params_string, %{})
  end

  def parse_params(_type, _params), do: %{}

  def parse_headers(header_lines) do
    Enum.reduce(header_lines, %{}, fn line, acc ->
      [key, value] = String.split(line, ": ")
      Map.put(acc, key, value)
    end)
  end
end
