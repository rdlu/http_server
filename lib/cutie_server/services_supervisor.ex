defmodule CutieServer.ServicesSupervisor do
  use Supervisor

  alias CutieServer.Services

  defdelegate puts(msg), to: CutieServer.Plugins

  def start_link(_arg) do
    puts("Starting the services supervisor...")
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    children = [
      Services.Pledger,
      {Services.SensorServer, 60}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
