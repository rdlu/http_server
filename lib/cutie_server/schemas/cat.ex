defmodule CutieServer.Schemas.Cat do
  defstruct id: nil, name: "", type: "", sleeping: false

  def is_mixed(%__struct__{} = cat) do
    cat.type |> String.starts_with?("Mixed")
  end

  def by_id(%__struct__{} = cat, id) do
    cat.id == id
  end
end
