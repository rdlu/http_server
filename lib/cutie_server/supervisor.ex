defmodule CutieServer.Supervisor do
  use Supervisor

  defdelegate puts(msg), to: CutieServer.Plugins

  def start_link(_arg) do
    puts("Starting the chief supervisor...")
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    children = [
      CutieServer.Kickstarter,
      CutieServer.ServicesSupervisor
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
