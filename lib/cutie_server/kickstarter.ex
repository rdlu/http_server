defmodule CutieServer.Kickstarter do
  defdelegate puts(msg), to: CutieServer.Plugins

  use GenServer

  def start_link(_arg) do
    puts("Starting the kickstarter...")
    GenServer.start(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Process.flag(:trap_exit, true)
    server_pid = start_server()
    {:ok, server_pid}
  end

  # Handles HttpServer crashes with unknown messages
  # Restarts the server
  def handle_info({:EXIT, _pid, reason}, _state) do
    puts("HttpServer exited (#{inspect(reason)})")
    server_pid = start_server()
    {:noreply, server_pid}
  end

  defp start_server do
    puts("Starting the HTTP server...")
    port = Application.get_env(:cutie_server, :port)
    server_pid = spawn_link(CutieServer.HttpServer, :start, [port])
    Process.register(server_pid, :http_server)
    server_pid
  end

  # Not used, but as a reminder
  def http_server_pid do
    Process.whereis(:http_server)
  end
end
