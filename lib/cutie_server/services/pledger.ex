defmodule CutieServer.Services.Pledger do
  @moduledoc """
  Pledger: A stateful repository of data
  Caches the recent data for faster retrieval
  """
  @process_name :pledge_server

  use GenServer

  defdelegate puts(msg), to: CutieServer.Plugins

  defmodule State do
    defstruct cache_size: 3, pledges: []
  end

  def start_link(_arg) do
    IO.puts("Starting the pledge server...")
    GenServer.start_link(__MODULE__, %State{}, name: @process_name)
  end

  # Client Interface
  def create_pledge(name, amount) do
    GenServer.call(@process_name, {:create_pledge, name, amount})
  end

  def recent_pledges do
    GenServer.call(@process_name, :recent_pledges)
  end

  def total_pledged do
    GenServer.call(@process_name, :total_pledged)
  end

  def clear do
    GenServer.cast(@process_name, :clear)
  end

  def set_cache_size(size) do
    GenServer.cast(@process_name, {:set_cache_size, size})
  end

  # Server Callbacks
  def init(state) do
    {:ok, %{state | pledges: fetch_recent_pledges_from_service()}}
  end

  def handle_call(:total_pledged, _from, state) do
    total = Enum.map(state.pledges, &elem(&1, 1)) |> Enum.sum()
    {:reply, total, state}
  end

  def handle_call(:recent_pledges, _from, state) do
    {:reply, state.pledges, state}
  end

  def handle_call({:create_pledge, name, amount}, _from, state) do
    {:ok, id} = send_pledge_to_service(name, amount)
    most_recent_pledges = Enum.take(state.pledges, state.cache_size - 1)
    new_pledges = [{name, amount} | most_recent_pledges]
    new_state = %{state | pledges: new_pledges}
    {:reply, id, new_state}
  end

  def handle_cast(:clear, state) do
    {:no_reply, %{state | pledges: []}}
  end

  def handle_cast({:set_cache_size, size}, state) do
    resized_cache = Enum.take(state.pledges, size)
    new_state = %{state | cache_size: size, pledges: resized_cache}

    {:no_reply, new_state}
  end

  def handle_info(message, state) do
    puts("Unknown message #{message}")
    {:no_reply, state}
  end

  # External Service Logic
  defp send_pledge_to_service(_name, _amount) do
    # CODE GOES HERE TO SEND PLEDGE TO EXTERNAL SERVICE
    {:ok, "pledge-#{:rand.uniform(1000)}"}
  end

  defp fetch_recent_pledges_from_service do
    # CODE GOES HERE TO FETCH RECENT PLEDGES FROM EXTERNAL SERVICE
    [{"wilma", 15}, {"fred", 25}]
  end
end
