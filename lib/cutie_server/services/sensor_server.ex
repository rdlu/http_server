defmodule CutieServer.Services.SensorServer do
  alias CutieServer.Repositories.Tracker
  alias CutieServer.Repositories.Snapshots

  @process_name :sensor_server
  @refresh_interval :timer.minutes(60)

  use GenServer

  defmodule State do
    defstruct sensor_data: %{},
              refresh_interval: :timer.minutes(60)
  end

  # Client Interface

  def start_link(interval) do
    IO.puts("Starting the sensor server with #{interval} min refresh...")
    initial_state = %State{refresh_interval: interval}
    GenServer.start_link(__MODULE__, initial_state, name: @process_name)
  end

  def start do
    GenServer.start(__MODULE__, %State{}, name: @process_name)
  end

  def get_sensor_data do
    GenServer.call(@process_name, :get_sensor_data)
  end

  def set_refresh_interval(time_in_ms) do
    GenServer.cast(@process_name, {:set_refresh_interval, time_in_ms})
  end

  # Server Callbacks

  def init(state) do
    sensor_data = fetch_sensor_data()
    initial_state = %{state | sensor_data: sensor_data}
    schedule_refresh()
    {:ok, initial_state}
  end

  def handle_cast({:set_refresh_interval, time_in_ms}, state) do
    new_state = %{state | refresh_interval: time_in_ms}
    {:noreply, new_state}
  end

  def handle_call(:get_sensor_data, _from, state) do
    {:reply, state.sensor_data, state}
  end

  def handle_info(:refresh, state) do
    IO.puts("Refreshing the cache...")
    sensor_data = fetch_sensor_data()
    new_state = %{state | sensor_data: sensor_data}
    schedule_refresh()
    {:noreply, new_state}
  end

  defp schedule_refresh do
    Process.send_after(self(), :refresh, @refresh_interval)
  end

  defp fetch_sensor_data do
    IO.puts("Running tasks to get sensor data...")

    task = Task.async(fn -> Tracker.get_location("Margot") end)

    snapshots =
      ["cam-1", "cam-2", "cam-3"]
      |> Enum.map(&Task.async(fn -> Snapshots.get_snapshot(&1) end))
      |> Enum.map(&Task.await/1)

    location = Task.await(task)

    %{snapshots: snapshots, location: location}
  end
end
