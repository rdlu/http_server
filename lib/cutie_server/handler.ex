defmodule CutieServer.Handler do
  @moduledoc """
  Handler: Handles the HTTP requests.
  It has a routing and response format responsability for simplicity.
  """
  import CutieServer.Plugins, only: [track_failures: 1, rewrite_path: 1]
  import CutieServer.Parser, only: [parse: 1]
  import CutieServer.FileHandler, only: [handle_file: 2]
  import CutieServer.RequestMap, only: :functions
  import CutieServer.View, only: [render: 3]

  alias CutieServer.RequestMap

  @pages_path Path.expand("pages", File.cwd!())

  @doc """
  Transforms the request modules into a response.
  It is similar to the Phoenix Framework "Pipeline"
  """
  def handle(request) do
    request
    |> parse()
    |> rewrite_path()
    |> route()
    |> track_failures()
    |> format_response()
  end

  # Pledges: Two stateless examples that caches the recent results
  def route(%RequestMap{method: "POST", path: "/pledges"} = map) do
    CutieServer.Controllers.Pledges.create(map, map.params)
  end

  def route(%RequestMap{method: "GET", path: "/pledges"} = map) do
    CutieServer.Controllers.Pledges.index(map)
  end

  # Sleep: An example where the server takes a while to answer
  # Used to develop initial concurrency
  def route(%RequestMap{method: "GET", path: "/sleep/" <> time} = map) do
    time |> String.to_integer() |> :timer.sleep()

    %RequestMap{map | status: 200, resp_body: "Awake!"}
  end

  # Crash: An example where something is raised and the server
  #        graciously drop the connection without crashing the entire server
  def route(%RequestMap{method: "GET", path: "/crash"}) do
    raise "Kaboom!"
  end

  # Padawan Parallel: Getting multiple data from another source that takes a while
  # It is done using a simplified async and on purpose more verbose on code.
  def route(%RequestMap{method: "GET", path: "/padawan"} = map) do
    prepare_snaps =
      Enum.map(1..5, fn x ->
        fn -> CutieServer.Repositories.Snapshots.get_snapshot("cam-#{x}") end
      end)

    # Non Blocking
    fetcher = CutieServer.Fetcher
    pids = Enum.map(prepare_snaps, &fetcher.async/1)
    snapshots = Enum.map(pids, &fetcher.get_result/1)
    # Blocking
    # snapshots = Enum.map(prepare_snaps, fn f -> f.() end)

    render(map, "sensors", snapshots: snapshots)
  end

  # Parallel: Getting multiple data from a slow source. Same as above with less code.
  # Uses Task for async/await and renders an eex template
  def route(%RequestMap{method: "GET", path: "/parallel"} = map) do
    snapshots =
      Enum.map(1..5, &"cam-#{&1}")
      |> Enum.map(&Task.async(fn -> CutieServer.Repositories.Snapshots.get_snapshot(&1) end))
      |> Enum.map(&Task.await/1)

    render(map, "sensors", snapshots: snapshots)
  end

  # Pets: Simple route for initial testing
  def route(%RequestMap{method: "GET", path: "/pets"} = map) do
    %RequestMap{map | resp_body: "Cats and Dogs", status: 200}
  end

  # Cats: Delegating to a separate controller
  def route(%RequestMap{method: "GET", path: "/cats"} = map) do
    CutieServer.Controllers.Cats.index(map)
  end

  # Api.Cats: Delegating to a separate controller, JSON version
  def route(%RequestMap{method: "GET", path: "/api/cats"} = map) do
    CutieServer.Api.Cats.index(map)
  end

  # Cats/Show: Delegating to a separate controller, pattern match on maps
  def route(%RequestMap{method: "GET", path: "/cats/" <> id} = map) do
    params = Map.put(map.params, "id", id)
    CutieServer.Controllers.Cats.show(map, params)
  end

  # Cats/Delete: Same as above, using another HTTP verb with different status code
  def route(%RequestMap{method: "DELETE", path: "/cats/" <> id} = map) do
    params = Map.put(map.params, "id", id)
    CutieServer.Controllers.Cats.delete(map, params)
  end

  # About: Reading a simple static file
  def route(%RequestMap{method: "GET", path: "/about"} = map) do
    @pages_path
    |> Path.join("about.html")
    |> File.read()
    |> handle_file(map)
  end

  # Slash: Initial Route
  # TODO: render README.md
  def route(%RequestMap{method: "GET", path: "/"} = map) do
    @pages_path
    |> Path.join("index.html")
    |> File.read()
    |> handle_file(map)
  end

  # Cats/New: Rendering a simple file, simulates the insertion of data
  def route(%RequestMap{method: "GET", path: "/cats/new"} = map) do
    @pages_path
    |> Path.join("form.html")
    |> File.read()
    |> handle_file(map)
  end

  # Pages: renders any static file from pages folder
  def route(%RequestMap{method: "GET", path: "/pages/" <> file} = map) do
    @pages_path
    |> Path.join(file <> ".html")
    |> File.read()
    |> handle_file(map)
  end

  # Cats/Create: simulates the creation of a new resource
  def route(%RequestMap{method: "POST", path: "/cats"} = map) do
    CutieServer.Controllers.Cats.create(map, map.params)
  end

  # Api.Cats/Create: simulates the creation of a new resource, JSON version
  def route(%RequestMap{method: "POST", path: "/api/cats"} = map) do
    CutieServer.Api.Cats.create(map, map.params)
  end

  def route(%RequestMap{method: "GET", path: "/pledges/new"} = map) do
    CutieServer.Controllers.Pledges.new(map)
  end

  def route(%RequestMap{method: "GET", path: "/sensors"} = map) do
    sensor_data = CutieServer.Services.SensorServer.get_sensor_data()

    %{map | status: 200, resp_body: inspect(sensor_data)}
  end

  # 404
  def route(%RequestMap{path: path} = map) do
    %RequestMap{map | resp_body: "No path for #{path}", status: 404}
  end

  defp format_response(%RequestMap{} = map) do
    """
    HTTP/1.1 #{full_status(map)}\r
    Content-Type: #{map.resp_headers["Content-Type"]}\r
    Content-Length: #{map.resp_body |> byte_size}\r
    \r
    #{map.resp_body}
    """
  end
end
