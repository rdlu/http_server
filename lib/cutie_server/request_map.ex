defmodule CutieServer.RequestMap do
  defstruct method: "",
            path: "",
            resp_body: "",
            resp_headers: %{"Content-Type" => "text/html"},
            status: nil,
            params: %{},
            headers: %{}

  def full_status(map) do
    "#{map.status} #{map.status |> status_reason}"
  end

  defp status_reason(code) do
    %{
      200 => "OK",
      201 => "Created",
      401 => "Unauthorized",
      403 => "Forbidden",
      404 => "Not Found",
      500 => "Internal Server Error"
    }[code]
  end
end
