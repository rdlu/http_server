defmodule CutieServer.Fetcher do
  @moduledoc """
  A simplification of Task module from elixir for learning purposes
  """
  def async(fun) do
    parent = self()

    spawn(fn -> send(parent, {self(), fun.()}) end)
  end

  def get_result(pid) do
    receive do
      {^pid, value} ->
        value
    after
      2000 ->
        raise "Timed out!"
    end
  end
end
