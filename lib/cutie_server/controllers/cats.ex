defmodule CutieServer.Controllers.Cats do
  alias CutieServer.RequestMap
  alias CutieServer.Repositories.CatCafe
  alias CutieServer.Views.Cats

  def index(%RequestMap{} = map) do
    cats = CatCafe.list_cats()

    render(map, cats, &Cats.index/1)
  end

  def show(%RequestMap{} = map, %{"id" => id}) do
    cat = CatCafe.get_cat(id)

    render(map, cat, &Cats.show/1)
  end

  def create(%RequestMap{} = map, %{"name" => name}) do
    render(map, "Adopted a new cat! Welcome, #{name}!", %{}, 201)
  end

  def delete(%RequestMap{} = map, _params) do
    render(map, "Cats must never be deleted!", %{}, 403)
  end

  defp render(map, view, data, status \\ 200)

  defp render(%RequestMap{} = map, body, _, status) when is_binary(body) do
    %RequestMap{map | status: status, resp_body: body}
  end

  defp render(%RequestMap{} = map, data, view, status) when is_function(view) do
    %RequestMap{map | resp_body: view.(data), status: status}
  end
end
