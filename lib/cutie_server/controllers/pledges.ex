defmodule CutieServer.Controllers.Pledges do
  alias CutieServer.RequestMap
  alias CutieServer.Services.Pledger

  import CutieServer.View

  def create(%RequestMap{} = map, %{"name" => name, "amount" => amount}) do
    # Sends the pledge to the external service and caches it
    Pledger.create_pledge(name, String.to_integer(amount))

    %RequestMap{map | status: 201, resp_body: "#{name} pledged #{amount}!"}
  end

  def index(%RequestMap{} = map) do
    # Gets the recent pledges from the cache
    pledges = Pledger.recent_pledges()

    render(map, "pledges/recent_pledges", pledges: pledges)
  end

  def new(%RequestMap{} = map) do
    render(map, "pledges/new_pledge")
  end
end
