defmodule CutieServer.Emoji do
  alias CutieServer.RequestMap

  def emojify(%RequestMap{status: 200} = map) do
    emojies = String.duplicate("🎉", 2)
    body = emojies <> "\n" <> map.resp_body <> "\n" <> emojies

    %RequestMap{map | resp_body: body}
  end

  def emojify(%RequestMap{} = map), do: map
end
