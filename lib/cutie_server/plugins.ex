defmodule CutieServer.Plugins do
  require Logger
  alias CutieServer.RequestMap

  def log(map) do
    if Mix.env() == :dev do
      Logger.info(inspect(map))
    end

    map
  end

  def puts(msg) do
    if Mix.env() == :dev do
      IO.puts(msg)
    end
  end

  def rewrite_path(%RequestMap{path: "/cuties"} = map) do
    %{map | path: "/pets"}
  end

  def rewrite_path(%RequestMap{path: path} = map) do
    regex = ~r{\/(?<family>\w+)\?id=(?<id>\d+)}
    captures = Regex.named_captures(regex, path)
    rewrite_path_captures(map, captures)
  end

  def rewrite_path(%RequestMap{} = map), do: map

  def rewrite_path_captures(%RequestMap{} = map, %{"family" => family, "id" => id}) do
    %RequestMap{map | path: "/#{family}/#{id}"}
  end

  def rewrite_path_captures(%RequestMap{} = map, nil), do: map

  def track_failures(%RequestMap{status: 404, path: path} = map) do
    if Mix.env() != :test do
      Logger.warn("Warning: missing #{path}")
    end

    map
  end

  def track_failures(%RequestMap{} = map), do: map
end
