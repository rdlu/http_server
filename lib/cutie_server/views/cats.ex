defmodule CutieServer.Views.Cats do
  require EEx

  @templates_path Path.expand("templates/cats", File.cwd!())

  EEx.function_from_file(:def, :index, Path.join(@templates_path, "index.html.eex"), [:cats])

  EEx.function_from_file(:def, :show, Path.join(@templates_path, "show.html.eex"), [:cat])
end
