defmodule CutieServer do
  use Application

  defdelegate puts(msg), to: CutieServer.Plugins

  def start(_type, _args) do
    puts("Starting the Cutie Server Application")
    CutieServer.Supervisor.start_link(%{})
  end
end
